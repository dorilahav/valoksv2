import express from 'express';

export default () => {
  const app = express();

  return new Promise(resolve => {
    app.listen(3000, () => {
      resolve(3000);
    });
  });
};