import startWebsite from './config/express';

startWebsite()
  .then(port => {
    console.log(`Website has started on port ${port}!`);
  })