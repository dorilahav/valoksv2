module.exports = {
    chainWebpack: config => {
        config.module
            .rule('eslint')
            .use('eslint-loader')
            .options({
                fix: true
            });
    },
    devServer: {
        proxy: {
            '/auth': {
                'target': 'http://localhost:3000',
                'changeOrigin': true
            },
            '/api': {
                'target': 'http://localhost:3000',
                'changeOrigin': true
            }
        }
    },
    transpileDependencies: [
        'vuetify'
    ]
}
