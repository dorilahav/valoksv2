import Vue from 'vue';
import store from '@/store';
import VueRouter, { RouteConfig } from 'vue-router';
import HomePage from '@/views/HomePage.vue';
import ServersPage from '@/views/ServersPage.vue';
import PlansPage from '@/views/PlansPage.vue';
import LoginPage from '@/views/LoginPage.vue';
import ServerPage from '@/views/ServerPage.vue';
import ManageServerPage from '@/views/ManageServerPage.vue';
import loggedMiddleware from '@/middlewares/logged';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: HomePage
  },
  {
    path: '/servers',
    name: 'Servers',
    component: ServersPage
  },
  {
    path: '/servers/:id',
    name: 'Server',
    component: ServerPage
  },
  {
    path: '/plans',
    name: 'Plans',
    component: PlansPage,
    meta: {
      requiredLoginState: false // TODO: make true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginPage,
    meta: {
      requiredLoginState: false
    }
  },
  {
    path: '/manage/:id',
    name: 'ManageServer',
    component: ManageServerPage,
    meta: {
      requiredLoginState: false // TODO: make true
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const { requiredLoginState } = to.meta;

  if (requiredLoginState !== undefined) {
    if (requiredLoginState) {
      loggedMiddleware({
        next,
        store
      });
    } else {
      next(); // TODO: Ensure guest
    }

    return;
  }

  next();
});

export default router;
