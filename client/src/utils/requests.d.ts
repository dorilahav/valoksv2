export interface HttpResponse {
    status: number;
}

export interface RequestData {
    headers?: RequestHeaders;
    parameters?: RequestParameters;
    body?: RequestBody;
}

export type RequestHeaders = { [key: string]: string };
export type RequestParameters = { [key: string]: string };
export type RequestBody = { [key: string]: any };
