import { HttpResponse } from './requests.d';

export interface VerifyLoginApiResponse extends HttpResponse {
    token: string;
}
