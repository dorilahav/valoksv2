import axios, {AxiosResponse} from 'axios';

import {
    RequestData,
    RequestParameters,
    RequestHeaders,
    RequestBody,
    HttpResponse
} from './requests.d';

export enum RequestMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    PATCH = 'PATCH',
    DELETE = 'DELETE'
}

export class HttpError extends Error {
    status: number;

    constructor(message: string, status: number) {
        super(message);
        this.status = status;
    }
}

async function sendRequest(method: RequestMethod, url: string, data?: RequestData): Promise<HttpResponse> {
    return await axios.request({
        url,
        method,
        params: data?.parameters,
        data: data?.body,
        headers: data?.headers
    }).then((response: AxiosResponse) => {
        const { error } = response.data;

        if (error) {
            throw new HttpError(error, response.status);
        }

        return {
            status: response.status,
            ...data
        };
    })
}

export function sendGet(url: string, parameters?: RequestParameters, headers?: RequestHeaders) {
    return sendRequest(RequestMethod.GET, url, {
        headers,
        parameters
    });
}

export function sendPost(url: string, body?: RequestBody, headers?: RequestHeaders) {
    return sendRequest(RequestMethod.POST, url, {
        headers,
        body
    });
}

export function sendPut(url: string, body?: RequestBody, headers?: RequestHeaders) {
    return sendRequest(RequestMethod.PUT, url, {
        headers,
        body
    });
}

export function sendPatch(url: string, body?: RequestBody, headers?: RequestHeaders) {
    return sendRequest(RequestMethod.PATCH, url, {
        headers,
        body
    });
}

export const sendDelete = (url: string, parameters?: RequestParameters, headers?: RequestHeaders) => {
    return sendRequest(RequestMethod.DELETE, url, {
        headers,
        parameters
    });
}
