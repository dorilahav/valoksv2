import {
    VerifyLoginApiResponse
} from './api.d';

import * as requests from '@/utils/requests';


export const verifyLogin = async (code: string) => {
    const response = await requests.sendGet('/api/users/verifyLogin', { code });

    return (response as VerifyLoginApiResponse).token;
}
