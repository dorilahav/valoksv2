import {Store} from "vuex";
import {NavigationGuardNext} from "vue-router";
import {Vue} from "vue/types/vue";

export type Middleware = {
    next: NavigationGuardNext<Vue>,
    store: Store<any>
}
