import {Middleware} from '@/middlewares/middleware';

export default ({ next, store }: Middleware) => {
    if (store.getters.isLoggedIn) {
        next();
        return;
    }

    next({ name: 'Login' });
}
