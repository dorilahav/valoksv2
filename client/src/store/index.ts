import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

interface IStore {
  state: {
    dark: boolean;
    token?: string;
    user?: object;
  };
  getters: {
    isLoggedIn: (state) => boolean;
  };
  mutations: {
    SET_TOKEN: (state, token: string) => void;
  };
  actions: {
    setToken: (ctx, token: string) => void;
    resetToken: (ctx) => void;
  };
}

const store: IStore = {
  state: {
    dark: true,
    token: undefined,
    user: undefined
  },
  getters: {
    isLoggedIn(state) {
      return state.token != null;
    }
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    }
  },
  actions: {
    setToken({ commit }, token) {
      commit('SET_TOKEN', token);
    },
    resetToken({ commit }) {
      commit('SET_TOKEN', undefined);
    }
  }
}

export default new Vuex.Store(store);
