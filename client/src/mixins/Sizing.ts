import Vue from 'vue'
import Component from 'vue-class-component';

@Component
export default class SizingMixin extends Vue {
    get mobile(): boolean {
        return this.$vuetify.breakpoint.xs;
    }
}
