export interface ISortOption {
    display: string;
    field: string;
    direction: 1 | -1;
}

export const sortOptions: ISortOption[] =  [
    {
        display: 'Name',
        field: 'name',
        direction: 1
    }, {
        display: 'Average Joins',
        field: 'averageJoins',
        direction: -1
    }, {
        display: 'Price: Highest to Lowest',
        field: 'price',
        direction: 1
    }, {
        display: 'Price: Lowest to Highest',
        field: 'price',
        direction: -1
    }
];
