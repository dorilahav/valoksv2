export interface IPlan {
    id: string;
    name: string;
    price: number;
    description: Array<string> | string;
    colors: IPlanColors;
    weight: number;
}

export interface IPlanColors {
    separator: string;
    gradient: GradientArray;
}

export type GradientArray = [string, string];