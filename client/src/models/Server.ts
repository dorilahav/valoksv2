export interface IServer {
    id: string;
    name: string;
    description?: string;
    icon: string;
    inviteCode: string;
    averageJoins: number;
    price: number;
    members: IMemberCount;
    plans?: Array<IServerPlan>;
}

export interface IMemberCount {
    total: number;
    online: number;
}

export interface IServerPlanDetails {
    name: string;
    description: string;
    price: number;
}

export interface IServerPlan extends IServerPlanDetails {
    id: string;
    averageJoins: number;
}
