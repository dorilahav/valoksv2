import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import 'vuetify/dist/vuetify.min.css';
import Sizing from '@/mixins/Sizing';

Vue.config.productionTip = false;
Vue.mixin(Sizing);

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');
