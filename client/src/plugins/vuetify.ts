import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        options: {
            customProperties: true,
        },
        themes: {
            dark: {
                primary: '80e290',
                secondary: '40444b',
                accent: '2f3136'
            },
        },
    }
});
