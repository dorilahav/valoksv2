module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: [
        'plugin:vue/essential',
        'eslint:recommended',
        '@vue/typescript/recommended'
    ],
    parserOptions: {
        ecmaVersion: 2020
    },
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'vue/html-indent': ['error', 2],
        'vue/html-quotes': ['error', 'double', { 'avoidEscape': true }],
        'indent': ['error', 2],
        'quotes': ['error', 'single', { 'avoidEscape': true }],
        'vue/max-attributes-per-line': [
            'error', {
                'singleline': 4,
                'multiline': { 'max': 4, 'allowFirstLine': true }
            }
        ],
        '@typescript-eslint/interface-name-prefix': [2, 'always']
    }
}
