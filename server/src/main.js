require('dotenv/config');

import connectDatabase from './config/database';
import startWebsite from './config/express';


connectDatabase()
  .then(startWebsite)
  .then(() => {
    console.log('Website is up!');
  })
  .catch(error => {
    console.error('Could not start website!');
    console.error(error);
    // Send log using error parameter
  });