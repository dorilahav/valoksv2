export class AuthError extends Error {
  constructor(message) {
    super(message);
  }
}

export class MissingTokenError extends AuthError {
  constructor() {
    super('Please provide a token!');
  }
}

export class InvalidTokenError extends AuthError {
  constructor() {
    super('The token is invalid!');
  }
}
