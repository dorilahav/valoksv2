import express from 'express';
import bodyParser from 'body-parser';

import authRouter from '../auth';
import errorMiddleware from '../middlewares/error';

export default async () => {
  const app = express();

  const port = process.env.PORT || 3000;

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  
  app.use('/auth', authRouter);
  
  app.use(errorMiddleware);

  app.listen(port);
};
