import mongoose from 'mongoose';

export default async () => {
  const connectionString = process.env.DATABASE_URL;

  if (!connectionString) {
    throw new Error('DATABASE_URL is not an environment variable!');
  }

  await mongoose.connect(connectionString, {
    useUnifiedTopology: true,
    useNewUrlParser: true
  });
};