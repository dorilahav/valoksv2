import express from 'express';
import * as controller from '../controllers/auth';

const router = express.Router();

router.get('/login', controller.login);
router.get('/verify', controller.verify);

export default router;
