import mongoose from 'mongoose';
import { timeInSeconds } from '../../utils/time';

const discordTokenSchema = new mongoose.Schema({
  access: {
    type: String,
    required: true
  },
  refresh: {
    type: String,
    required: true
  },
  expiry: {
    type: Date,
    required: true
  }
}, { _id: false });


const discordSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  token: {
    type: discordTokenSchema,
    required: true
  }
}, { _id: false });


const schema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  discord: discordSchema
});

discordSchema.methods.updateToken = function (rawToken) {
  this.token = {
    access: rawToken.access_token,
    refresh: rawToken.refresh_token,
    expiry: timeInSeconds(rawToken.expires_in)
  };
};

export default mongoose.model('User', schema);
