
import User from './model';
import BaseLogic from '../logic';

export class UserLogic extends BaseLogic {
  constructor() {
    super(User);
  }

  async getByDiscordId(discordId) {
    return new Promise((resolve, reject) => {
      User.findOne({ discordId }, (err, user) => {
        if (err) {
          return reject(err);
        }

        resolve(user);
      });
    });
  }

  async create(email, discordId, token) {
    const user = new User({
      email,
      discord: {
        id: discordId
      }
    });

    user.discord.updateToken(token);

    await user.save();

    return user;
  }
}

export default UserLogic;