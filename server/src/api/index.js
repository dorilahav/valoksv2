import {Router} from 'express';
import {ensureAuthentication} from './middlewares/auth';

const router = new Router();

router.use(ensureAuthentication);

export default router;