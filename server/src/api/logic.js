export default class BaseLogic {
  constructor(model) {
    this._model = model;
  }

  async getById(id) {
    return await this._model.findById(id).exec();
  }
}
