import {InvalidTokenError, MissingTokenError} from '../../errors/auth';
import jwt from 'jsonwebtoken';
import UserLogic from '../users/logic';

const TOKEN_HEADER = 'Authorization';
const TOKEN_SECRET = process.env.AUTH_TOKEN_SECRET;

const decodeToken = token => new Promise((resolve, reject) => {
  jwt.verify(token, TOKEN_SECRET, (err, {data}) => {
    if (err || !data?.userId) {
      return reject(new InvalidTokenError());
    }

    resolve(data.userId);
  });
});

const getUserByToken = async token => {
  const userId = await decodeToken(token);

  return await new UserLogic().getById(userId);
};

export const ensureAuthentication = (req, res, next) => {
  const token = req.header(TOKEN_HEADER);

  if (token === undefined) {
    return next(new MissingTokenError());
  }

  return getUserByToken(token).then(user => {
    req.token = token;
    req.user = user;

    next();
  }).catch(next);
};
