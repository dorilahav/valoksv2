export const timeInSeconds = seconds => {
  const now = new Date();
  now.setSeconds(now.getSeconds() + seconds);

  return now;
};
