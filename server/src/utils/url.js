const parseParam = ([key, value]) => {
  const parsedValue = value instanceof Array ? value.map(x => x.toString()).join(',') : value.toString();

  return `${key}=${parsedValue}`;
};

const formatParams = params => Object.entries(params).filter(param => param).map(parseParam).join('&');

class Builder {
  constructor(baseUrl) {
    this._baseUrl = baseUrl;
  }

  params(params) {
    this._params = params;

    return this;
  }

  build() {
    return `${this._baseUrl}?${formatParams(this._params)}`;
  }
}

export default {
  Builder: baseUrl => new Builder(baseUrl)
};
