import { InvalidTokenError, MissingTokenError, AuthError } from '../errors/auth';

const handleAuthError = (error, res) => {
  if (error instanceof MissingTokenError) {
    res.status(400);
  }

  if (error instanceof InvalidTokenError) {
    res.status(401);
  }

  res.send(error.message);
};

export default (error, _req, res, _next) => {
  if (error instanceof AuthError) {
    return handleAuthError(error, res);
  }

  console.log(error);

  return res.status(500).send('An error has occurred!');
};
