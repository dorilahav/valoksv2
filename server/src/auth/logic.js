import qs from 'querystring';
import axios from 'axios';

import url from '../utils/url';
import UserLogic from '../api/users/logic';

const DISCORD_BASE_URL = 'https://discord.com/api/v6';
const DISCORD_OAUTH_BASE_URL = `${DISCORD_BASE_URL}/oauth2`;

const BASE_LOGIN_URL_PARAMS = {
  prompt: 'none',
  response_type: 'code'
};

const BASE_TOKEN_PARAMS = {
  grant_type: 'authorization_code'
};

const BASE_HEADERS = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

const REQUIRED_SCOPE = 'identify email guilds';

const buildLoginRedirectUrl = () => {
  const params = {
    client_id: process.env.DISCORD_CLIENT_ID,
    scope: REQUIRED_SCOPE,
    redirect_uri: process.env.DISCORD_LOGIN_REDIRECT_URL,
    ...BASE_LOGIN_URL_PARAMS,
  };

  return url.Builder(`${DISCORD_OAUTH_BASE_URL}/authorize`).params(params).build();
};

const exchangeCode = code => {
  const data = {
    client_id: process.env.DISCORD_CLIENT_ID,
    client_secret: process.env.DISCORD_CLIENT_SECRET,
    scope: REQUIRED_SCOPE,
    redirect_uri: process.env.DISCORD_LOGIN_REDIRECT_URL,
    code,
    ...BASE_TOKEN_PARAMS,
  };

  const headers = {
    ...BASE_HEADERS
  };

  return axios.post(`${DISCORD_OAUTH_BASE_URL}/token`, qs.stringify(data), {
    headers
  }).then(response => response.data);
};

const getDiscordUser = token => axios.get(`${DISCORD_BASE_URL}/users/@me`, {
  headers: { Authorization: `Bearer ${token.access_token}` }
}).then(response => response.data);

class DiscordAuthLogic {
  get redirectUrl() {
    if (!this._redirectUrl) {
      this._redirectUrl = buildLoginRedirectUrl();
    }

    return this._redirectUrl;
  }

  async getUserByCode(code) {
    const token = await exchangeCode(code);
    const discordUser = await getDiscordUser(token);
    const userLogic = new UserLogic();
    const user = await userLogic.getByDiscordId(discordUser.id);

    if (user) {
      return user;
    }

    return userLogic.create(discordUser.email, discordUser.id, token);
  }
}

export default DiscordAuthLogic;
