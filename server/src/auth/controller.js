import AuthLogic from './logic';

export const getRedirectUrl = (req, res) => res.send(new AuthLogic().redirectUrl);

export const login = (req, res, next) => {
  const { code } = req.body;

  return new AuthLogic().getUserByCode(code).then(user => res.send(user)).catch(next);
};
