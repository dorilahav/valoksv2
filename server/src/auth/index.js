import {Router} from 'express';
import {getRedirectUrl, login} from './controller';

const router = Router();

router.get('/redirect', getRedirectUrl);
router.post('/login', login);

export default router;