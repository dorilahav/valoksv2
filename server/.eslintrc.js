module.exports = {
  env: {
    es6: true,
    node: true
  },
  extends: [
    'eslint:recommended'
  ],
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 2020
  },
  ignorePatterns: ['dist'],
  rules: {
    semi: ['error', 'always'],
    quotes: ['error', 'single'],
    indent: ['error', 2],
    eqeqeq: 'error',
    'no-empty': 'error',
    'no-extra-semi': 'error',
    'no-setter-return': 'error',
    'no-unreachable': 'error',
    'dot-location': ['error', 'property'],
    'no-else-return': 'error',
    'no-empty-function': 'error',
    'no-empty-pattern': 'error',
    'no-eval': 'error',
    'no-implicit-globals': 'error',
    'no-magic-numbers': 'warn',
    'no-multi-spaces': 'error',
    'no-octal': 'error',
    'no-param-reassign': 'error',
    'no-throw-literal': 'error',
    'no-unused-expressions': 'error',
    'no-var': 'error',
    'arrow-body-style': ['error', 'as-needed'],
    'arrow-parens': ['error', 'as-needed'],
    'arrow-spacing': 'error',
    'no-duplicate-imports': 'error',
    'object-shorthand': ['error', 'always', { avoidExplicitReturnArrows: true }],
    'prefer-arrow-callback': 'error',
    'prefer-const': 'error',
    'prefer-destructuring': 'warn',
    'prefer-template': 'error'
  }
};