import React, { FunctionComponent } from 'react';
import BlobDesign from './BlobDesign';
import { Box, Grid, makeStyles, Typography, Button } from '@material-ui/core';
import MapBackground from '../../assets/img/background.png';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    backgroundSize: 'cover',
    backgroundPositionX: 'center',
    backgroundPositionY: 'center',
    backgroundImage: `url(${MapBackground})`,
    backgroundRepeat: 'no-repeat',
    '&::before': {
      position: 'absolute',
      height: '100%',
      width: '100%',
      backgroundColor: '#32353b',
      opacity: 0.98,
      content: '""'
    }
  },
  button: {
    padding: '15px 0',
    fontSize: '2rem',
    margin: 10,
    width: 320
  }
});

const Home: FunctionComponent = () => {

  const classes = useStyles();

  return (
    <Box display="flex" justifyContent="center" alignItems="center" flex="1" position="relative" className={classes.root}>
      <BlobDesign />
      <Box display="flex" flexDirection="column" zIndex="1" textAlign="center">
        <Box>
          <Typography variant="h2">514,318 members</Typography>
          <Typography variant="h2">2,132 servers</Typography>
        </Box>
        <Grid container justify="center">
          <Grid item>
            <Button component={Link} variant="contained" color="secondary" className={classes.button} to="/servers">Start Buying</Button>
          </Grid>
          <Grid item>
            <Button component={Link} variant="contained" color="secondary" className={classes.button} to="/plans">Start Selling</Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default Home;