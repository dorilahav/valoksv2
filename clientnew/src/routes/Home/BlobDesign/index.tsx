import React, { FunctionComponent } from 'react';
import { Box } from '@material-ui/core';

import Art1 from '../../../assets/img/art-1.png';
import Art2 from '../../../assets/img/art-2.png';
import Art3 from '../../../assets/img/art-3.png';
import Art4 from '../../../assets/img/art-4.png';
import Art5 from '../../../assets/img/art-5.png';
import Art6 from '../../../assets/img/art-6.png';

const BlobDesign: FunctionComponent = () => (
  <Box position="absolute" height="100%" width="100%">
    <img
      src={Art1}
      alt="10"
      width="22%"
      style={{position: 'absolute', top: 0, left: 0}}
    />
    <img
      src={Art4}
      alt="10"
      width="20%"
      style={{position: 'absolute', top: 0, right: '8vw'}}
    />
    <img
      src={Art3}
      alt="10"
      width="11%"
      style={{position: 'absolute', top: 0, right: '41vw'}}
    />
    <img
      src={Art5}
      alt="10"
      width="24%"
      style={{position: 'absolute', bottom: 0, right: 0}}
    />
    <img
      src={Art6}
      alt="10"
      width="11%"
      style={{position: 'absolute', bottom: 0, left: '65vw'}}
    />
    <img
      src={Art2}
      alt="10"
      width="25%"
      style={{position: 'absolute', bottom: 0, left: '5vw'}}
    />
  </Box>
);

export default BlobDesign;