import React, { FunctionComponent } from 'react';
import {Switch, Route} from 'react-router-dom';

import Home from './Home';
import Login from './Login';

const Routes: FunctionComponent = () => (
  <Switch>
    <Route exact path="/" component={Home}/>
    <Route exact path="/login" component={Login}/>
  </Switch>
);

export default Routes;