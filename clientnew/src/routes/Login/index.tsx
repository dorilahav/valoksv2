import React, { FunctionComponent, useEffect } from 'react';
import { Typography, Box, CircularProgress } from '@material-ui/core';
import {RouteProps} from 'react-router';
import qs from 'querystring';
import { useAuth } from '../../hooks';

const Login: FunctionComponent<RouteProps> = ({location}) => {
  const {login} = useAuth();

  const getCode = () => {
    const query = qs.parse(location.search.substring(1));

    return query.code;
  }

  useEffect(() => {
    const code = getCode() as string;

    login(code);
  }, []);

  return (
    <Box display="flex" flexDirection="column" alignItems="center" padding={15}>
      <Box marginBottom={3}>
        <Typography variant="h1">Logging you in...</Typography>
      </Box>
      <Box marginTop={3}>
        <CircularProgress color="primary" size={75} />
      </Box>
    </Box>
  );
};

export default Login;