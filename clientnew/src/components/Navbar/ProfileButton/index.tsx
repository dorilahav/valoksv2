import React, { FunctionComponent, useState, useRef } from 'react';
import { IconButton, makeStyles } from '@material-ui/core';
import { IUser } from '../../../hooks/auth';
import ProfileMenu from './ProfileMenu';

interface IProps {
  user: IUser;
}

const useStyles = makeStyles({
  avatar: {
    borderRadius: '50%'
  }
});

const ProfileButton: FunctionComponent<IProps> = ({user}) => {
  const classes = useStyles();

  const [isMenuOpen, setMenuOpen] = useState(false);
  const buttonRef = useRef(null);

  return (
    <>
      <IconButton ref={buttonRef} onClick={() => setMenuOpen(true)}>
        <img src={user.avatar} height={40} width={40} className={classes.avatar}/>
      </IconButton>
      <ProfileMenu user={user} anchorElement={buttonRef.current} open={isMenuOpen} onClose={() => setMenuOpen(false)} />
    </>
  );
};

export default ProfileButton;