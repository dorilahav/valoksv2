import React, { FunctionComponent } from 'react';
import { Menu, MenuItem, Typography, Divider, makeStyles } from '@material-ui/core';
import { IUser, useAuth } from '../../../../hooks/auth';

interface IProps {
  anchorElement: HTMLElement;
  open: boolean;
  onClose: () => void;
  user: IUser;
};

const useStyles = makeStyles({
  divider: {
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    margin: '3px 8px'
  }
});

const ProfileMenu: FunctionComponent<IProps> = ({user, anchorElement, open, onClose}) => {
  const classes = useStyles();

  const {logout} = useAuth();

  return (
    <Menu anchorEl={anchorElement} anchorOrigin={{ vertical: "bottom", horizontal: "center" }} getContentAnchorEl={null} open={open} onClose={onClose} keepMounted>
      <MenuItem>
        <Typography variant="h6">{user.username}</Typography>
      </MenuItem>
      <Divider className={classes.divider} />
      <MenuItem onClick={logout}>Logout</MenuItem>
    </Menu>
  );
};

export default ProfileMenu;