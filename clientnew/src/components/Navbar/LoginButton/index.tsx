import React, { FunctionComponent } from 'react'
import {IconButton, makeStyles} from '@material-ui/core';
import {LockOpen} from '@material-ui/icons';
import { useAuth } from '../../../hooks';

const useStyles = makeStyles({
  root: {
    color: 'white',
    '&:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.04)'
    }
  }
});

const LoginButton: FunctionComponent = () => {
  const classes = useStyles();
  const {redirect:redirectToLogin} = useAuth();

  return (
    <IconButton className={classes.root} onClick={redirectToLogin}>
      <LockOpen />
    </IconButton>
  )
}

export default LoginButton;
