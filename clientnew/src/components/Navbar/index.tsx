import React, { FunctionComponent } from 'react';
import {AppBar, Toolbar, Box, makeStyles, colors, IconButton} from '@material-ui/core';
import Icon from '../../assets/img/icon.png';
import LoginButton from './LoginButton';
import {Link} from 'react-router-dom';
import ProfileButton from './ProfileButton';
import { useAuth } from '../../hooks';

const useStyles = makeStyles({
  root: {
    backgroundColor: colors.grey[900]
  }
});

const Navbar: FunctionComponent = () => {
  const classes = useStyles();
  const {user} = useAuth();

  return (
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        <Box width="100%" display="flex" flexDirection="row" alignItems="center" justifyContent="space-between">
          <IconButton component={Link} disableRipple to="/">
            <img src={Icon} width={40} height={40} />
          </IconButton>
          {!!user ? <ProfileButton user={user}/> : <LoginButton />}
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;