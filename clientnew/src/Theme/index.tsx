
import React, { FunctionComponent } from 'react';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#80e290'
    },
    secondary: {
      main: '#272727'
    },
    background: {
      default: '#36393f',
      paper: '#2f3136'
    },
    text: {
      primary: '#ffffff',
      secondary: '#f50057'
    }
  }
});

const Theme: FunctionComponent = ({children}) => (
  <MuiThemeProvider theme={theme}>
    {children}
  </MuiThemeProvider>
);

export default Theme;