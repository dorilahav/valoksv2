import React, {createContext, useState, FunctionComponent} from 'react';

export const UserContext = createContext(null);
export const SetUserContext = createContext(null);

export const UserProvider: FunctionComponent = ({children}) => {
  const [user, setUser] = useState(null);

  return (
    <UserContext.Provider value={user}>
      <SetUserContext.Provider value={setUser}>
        {children}
      </SetUserContext.Provider>
    </UserContext.Provider>
  );
};