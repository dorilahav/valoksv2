import {useRequest} from '../request';
import { useContext, useEffect } from 'react';
import { UserContext, SetUserContext } from '../../context/User';

const requests = useRequest();
const storage: Storage = process.env.NODE_ENV === 'production' ? localStorage : sessionStorage;

export type IUser = {
  id: string;
  username: string;
  avatar: string;
}

const redirectToLogin = async () => {
  const redirectUrl = await requests.get<string>('/auth/redirect').then(response => response.data);

  window.location.href = redirectUrl;
}

const sendLoginRequest = async <TUser extends IUser>(code: string): Promise<TUser> => {
  const user = await requests.post<TUser>('/auth/login', {code}).then(response => response.data);

  storage.user = JSON.stringify(user);

  return user;
}

const removeUserFromStorage = () => {
  delete storage.user;
};

const getUserFromStorage = <TUser extends IUser>(): TUser => {
  const user = storage?.user;

  return user && JSON.parse(user);
}

export const useAuth = <TUser extends IUser>() => {
  const user = useContext<TUser>(UserContext);
  const setUser = useContext(SetUserContext);

  useEffect(() => {
    setUser(getUserFromStorage());
  }, []);

  const login = (code: string) =>
    sendLoginRequest(code)
      .then(user => setUser(user));

  const logout = () => {
    removeUserFromStorage();
    setUser(null);
  }

  return {
    redirect: redirectToLogin,
    login,
    logout,
    user
  };
};