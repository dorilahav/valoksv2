import axios, { AxiosResponse, AxiosRequestConfig } from 'axios';

export type Response<T = any> = {
  data: T;
  status: number;
  headers: Record<string, string>;
}

export enum RequestMethod {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE'
}

export type RequestConfig = {
  headers?: Record<string, string>;
  params?: Record<string, string>;
  data?: Record<string, any>;
  method?: RequestMethod;
  timeout?: number;
}

const parseConfig = ({headers, params, data, method, timeout}: RequestConfig = {}): AxiosRequestConfig => ({headers, params, data, method, timeout});
const parseResponse = <T = any>({data, status, headers}: AxiosResponse<T>): Response<T> => ({data, status, headers});

const sendGetRequest = <T = any>(url: string, config?: RequestConfig): Promise<Response<T>> =>
  axios.get(url, parseConfig(config)).then(parseResponse)

const sendPostRequest = <T = any>(url: string, data?: any, config?: RequestConfig): Promise<Response<T>> =>
  axios.post(url, data, parseConfig(config)).then(parseResponse);

const sendDeleteRequest = <T = any>(url: string, config?: RequestConfig): Promise<Response<T>> =>
  axios.delete(url, parseConfig(config)).then(parseResponse);

const sendPutRequest = <T = any>(url: string, data?: any, config?: RequestConfig): Promise<Response<T>> =>
  axios.put(url, data, parseConfig(config)).then(parseResponse);

const sendPatchRequest = <T = any>(url: string, data?: any, config?: RequestConfig): Promise<Response<T>> =>
  axios.patch(url, data, parseConfig(config)).then(parseResponse);

export const useRequest = () => ({
  get: sendGetRequest,
  post: sendPostRequest,
  delete: sendDeleteRequest,
  put: sendPutRequest,
  patch: sendPatchRequest
});