import React, { FunctionComponent } from 'react';
import {Box} from '@material-ui/core';
import {BrowserRouter as Router} from 'react-router-dom';

import Navbar from './components/Navbar';
import Routes from './routes';
import { UserProvider } from './context/User';

const App: FunctionComponent = () => (
  <Router>
    <Box display="flex" flexDirection="column" height="100%">
      <UserProvider>
        <Navbar />
        <Routes />
      </UserProvider>
    </Box>
  </Router>
);

export default App;